import React, { useState } from "react";

import "./ExpenseForm.css";

const ExpenseForm = (props) => {
  // Single state multiple times
  const [enteredTitle, setEnteredTitle] = useState("");
  const [enteredAmount, setEnteredAmount] = useState("");
  const [enteredDate, setEnteredDate] = useState("");

  // One state approach
  /*
  const [userInput, setUserInput] = useState({
    enteredTitle: "",
    enteredAmount: "",
    enteredDate: "",
  });
  */

  const titleChangeHandler = (evt) => {
    /******************************************* */
    /* ******* SINGLE STATE APPROACH *********** */
    /******************************************* */

    setEnteredTitle(evt.target.value);

    /******************************************* */
    /* ********** ONE STATE APPROACH *********** */
    /******************************************* */

    // setUserInput({
    //     ...userInput,
    //     enteredTitle: evt.target.value
    // });

    /******************************************* */
    /* ****** ONE STATE APPROACH *************** */
    /******** WITH ALWAYS UP-TO-DATE STATE ***** */
    /******************************************* */

    // setUserInput((prevState) => {
    //   return {
    //     ...prevState,
    //     enteredTitle: evt.target.value,
    //   };
    // });
  };

  const amountChangehandler = (evt) => {
    setEnteredAmount(evt.target.value);
  };

  const dateChangehandler = (evt) => {
    setEnteredDate(evt.target.value);
  };

  const onFormSubmitted = (evt) => {
    evt.preventDefault();

    const expenseData = {
      title: enteredTitle,
      amount: enteredAmount,
      date: new Date(enteredDate),
    };
    props.onSaveExpenseData(expenseData);
    // Clear form values
    setEnteredTitle('');
    setEnteredAmount('');
    setEnteredDate('');
  };

  return (
    <form onSubmit={onFormSubmitted}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Title</label>
          <input
            type="text"
            value={enteredTitle}
            onChange={titleChangeHandler}
          />
        </div>
        <div className="new-expense__control">
          <label>Amount</label>
          <input
            type="number"
            value={enteredAmount}
            min="0.01"
            step="0.01"
            onChange={amountChangehandler}
          />
        </div>
        <div className="new-expense__control">
          <label>Date</label>
          <input
            type="date"
            min="2021-01-01"
            max="2022-12-31"
            onChange={dateChangehandler}
            value={enteredDate}
          />
        </div>
      </div>
      <div className="new-expense__actions">
        <button type="submit">Add Expense</button>
      </div>
    </form>
  );
};

export default ExpenseForm;
