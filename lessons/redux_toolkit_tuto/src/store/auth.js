import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
  isAuth: false,
};

const authSlice = createSlice({
  name: "authentication",
  initialState: initialAuthState,
  reducers: {
    login(currentState) {
      currentState.isAuth = true;
    },
    logout(currentState) {
      currentState.isAuth = false;
    },
  },
});
export const authActions = authSlice.actions;

export default authSlice.reducer;
