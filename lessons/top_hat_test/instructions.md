# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions:

Since this project was created using `create-react-app` it requires node.

If node is not installed, please download it and install it from the following site: https://nodejs.org/en/.

Once `nodejs`is installed, you can run the following commands

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
