import { useSelector } from 'react-redux';

import { Fragment } from "react";
import "./App.css";

import SearchForm from "./components/searchForm";
import CoursesList from "./components/coursesList";

function App() {
  const coursesList = useSelector((state) => state.courses.coursesList);
  
  return (
    <Fragment>
      <div className="App">
        <div className="container">
          <SearchForm />
          <CoursesList list={coursesList} />
        </div>
      </div>
    </Fragment>
  );
}

export default App;
