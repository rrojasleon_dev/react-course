import axios from "axios";

import CONSTANTS from '../utils/constants';

// API endpoint. Normally, i would  put this constant into a separate file. but since it's just 1 endpooint, no need.

export default class RestService {
  /**
   * Rest constructor
   * @param requestParams
   * @returns {AxiosPromise}
   */
  constructor(requestParams) {
    return axios(requestParams);
  }

  // Internal methods.
  // Due to the siz of the app, just GET method will be present.
  /**
   *
   * @param payload
   * @returns {Promise<AxiosResponse>}
   */
  static get(endpoint, params) {
    return axios({
      method: "GET",
      url: endpoint,
      headers: { "Access-Control-Allow-Origin": "*" },
      params: new URLSearchParams(params)
    });
  }

  /**
   * Fetch dummy data from CONSTANTS file to simulate GET request.
   * @returns {Promise}
   */
  static getDummyData() {
    return new Promise((resolve, reject) => {
        // simulate response time
        const coursesList = CONSTANTS.coursesList;
        setTimeout(() => {
            resolve({
                data: coursesList
            });
        }, 1000);
    });
  }
}
