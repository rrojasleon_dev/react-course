import { configureStore } from '@reduxjs/toolkit';
// Store Slices
import coursesReducer from './courses';

const store = configureStore({
    reducer: {
        courses: coursesReducer
    }
});

export default store;