import { createSlice } from '@reduxjs/toolkit';

const initialCoursesState = {
    isSearching: false,
    hasError: false,
    errorMessage: null,
    coursesList: []
};
// Create store slice.
const coursesSlice = createSlice({
    name: 'courses',
    initialState: initialCoursesState,
    reducers: {
        setCourses(currentState, action) {
            currentState.coursesList = [...action.payload.courses];
        },
        resetCourses(currentState) {
            currentState.coursesList = [];
        },
        setErrorStatus(currentState, action) {
            currentState.hasError = action.payload.status;
        },
        setErrorMessage(currentState, action) {
            currentState.errorMessage = action.payload.message;
        },
        startSearch(currentState) {
            currentState.isSearching = true;
        },
        endSearch(currentState) {
            currentState.isSearching = false;
        }
    }
});

export const coursesActions = coursesSlice.actions;

export default coursesSlice.reducer;