import { React } from "react";

import { useSelector } from "react-redux";

import "./coursesList.module.css";
import LoadingStatus from "./loadingStatus";

const CoursesList = (props) => {
  // subcribe to state properties
  const isSearching = useSelector((state) => state.courses.isSearching);
  const hasErrors = useSelector(state => state.courses.hasError);
  // Render our tree.
  const renderSubNodes = (subNodes) => {
    return (
      <ul>
        {subNodes.map((node) => {
          return (
            <li key={node.id}>
              {node.name}
              {renderSubNodes(node.children)}
            </li>
          );
        })}
      </ul>
    );
  };

  const List = () => {
    return (
      <div>
        {props.list.length > 0 && renderSubNodes(props.list)}
      </div>
    );
  };

  return (
    <div>
      <LoadingStatus/>
      {!isSearching && !hasErrors && props.list.length > 0 && <List />}
      {!isSearching && !hasErrors && props.list.length < 1 && <p>No results found. Please use a different keyword</p>}
    </div>
  );
};

export default CoursesList;
