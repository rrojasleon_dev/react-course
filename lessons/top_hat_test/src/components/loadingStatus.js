import { React } from 'react';
import { useSelector } from 'react-redux';

const LoadingStatus = () => {
    const isSearching = useSelector(state => state.courses.isSearching);
    const hasError = useSelector(state => state.courses.hasError).toString();
    const errorMessage = useSelector(state => state.courses.errorMessage);

    return(
        <div>
            { isSearching && <p>Searching...</p>}
            { !isSearching && hasError && <p>{errorMessage}</p>}
        </div>
    );
};

export default LoadingStatus;