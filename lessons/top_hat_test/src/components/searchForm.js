import { React, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";

import { coursesActions } from "../store/courses";

import classes from "./searchForm.module.css";

import REST from "../services/rest.service";

const SearchForm = () => {
  // Ref for our input and get it's value.
  const searchRef = useRef();

  // Dispatcher for our state actios.
  const dispatcher = useDispatch();

  // Subscribing to state slice properties.
  const searchStatus = useSelector((state) => state.courses.isSearching);

  // Create the tree based on our provided list.
  const createTree = (list, id = 0) => {
    return list
      .filter((item) => item["parent_id"] === id)
      .map((it) => {
        return {
          ...it,
          children: createTree(list, it.id),
        };
      });
  };

  const setError = (errorStatus, errorMessage) => {
    dispatcher(coursesActions.setErrorStatus({ status: errorStatus }));
    dispatcher(coursesActions.setErrorMessage({ message: errorMessage }));
  };

  // Handle our form submit.
  const onFormSubmitHandler = (e) => {
    e.preventDefault();
    setError(false, null);
    dispatcher(coursesActions.startSearch());

    const searchTerm = searchRef.current.value;
    const params = {
      query: searchTerm,
    };

    REST.get("/treesearch/", params)
      .then((res) => {
        let errorMessage = "";
        switch (res.data.statusCode) {
          default:
            break;
          case 200:
            if (res.data.body.length > 0) {
              const treeList = createTree(res.data.body);
              dispatcher(coursesActions.setCourses({ courses: treeList }));
            } else {
              dispatcher(coursesActions.setCourses({ courses: [] }));
            }
            break;
          case 400:
            errorMessage = JSON.parse(res.data.body).error;
            setError(true, errorMessage);
            break;
          case 503:
            errorMessage = JSON.parse(res.data.body).error;
            setError(true, errorMessage);
            break;
        }
        dispatcher(coursesActions.endSearch());
      })
      .catch((error) => {
        if (error.message) {
          const errorMessage = error.message + ". Please Try Again";
          setError(true, errorMessage);
        }
        dispatcher(coursesActions.endSearch());
      });
  };

  // Reset Data.
  const onResetHandler = (e) => {
    e.preventDefault();
    dispatcher(coursesActions.resetCourses());
    searchRef.current.value = "";
  };

  return (
    <form className={classes.form} onSubmit={onFormSubmitHandler}>
      <input
        ref={searchRef}
        disabled={searchStatus}
        className={classes["search-input"]}
        type="text"
        placeholder="Search course"
      ></input>
      <div className={classes.controls}>
        <button disabled={searchStatus} type="submit">
          SEARCH
        </button>
        <button disabled={searchStatus} onClick={onResetHandler}>
          RESET
        </button>
      </div>
      <br />
    </form>
  );
};

export default SearchForm;
