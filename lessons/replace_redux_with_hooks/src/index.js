import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
// import { Provider } from "react-redux";
// import { combineReducers, createStore } from "redux";

import "./index.css";
import App from "./App";
import configureProductsStore from "./hooks-store/products-store";

configureProductsStore();
// import productReducer from "./store/reducers/products";
// import ProductsProdiver from "./context/products-context";

// const rootReducer = combineReducers({
//   shop: productReducer,
// });

// const store = createStore(rootReducer);

const root = ReactDOM.createRoot(document.getElementById("root"));
// root.render(
//   <Provider store={store}>
//     <BrowserRouter>
//       <App />
//     </BrowserRouter>
//   </Provider>
// );
// root.render(
//   <ProductsProdiver>
//     <BrowserRouter>
//       <App />
//     </BrowserRouter>
//   </ProductsProdiver>
// );
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);
