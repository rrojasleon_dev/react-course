import MainNavigation from "../components/MainNavigation";

function ErrorPage() {
  return (
    <>
      <MainNavigation />
      <main>
        <h1>An error occurred</h1>
        <p>Cold not load page</p>
      </main>
    </>
  );
}

export default ErrorPage;
