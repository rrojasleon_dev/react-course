import { useState } from "react";
import Output from "./Output";
const Greeting = () => {
    const [changedText, setTextChanged] = useState(false);

    const changeTextHandler = () => {
        setTextChanged(true);
    };

    return (
        <div>
            <h2>Title</h2>
            { !changedText && <Output>default text</Output>}
            { changedText && <Output>Changed</Output>}
            <button onClick={changeTextHandler}>Change text</button>
        </div>
    )
};

export default Greeting;