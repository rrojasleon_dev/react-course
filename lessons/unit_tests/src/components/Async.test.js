import { render, screen } from "@testing-library/react";
import Async from "./Async";

describe("Async component", () => {
  test("renders posts if request suceeds", async () => {
    // create a mock
    window.fetch = jest.fn();
    window.fetch.mockResolvedValueOnce({
      json: async () => [{ id: "p1", title: "post 1" }],
    });
    
    render(<Async />);

    const listItems = await screen.findAllByRole(
      "listitem",
      {},
      { timeout: 2000 }
    );

    expect(listItems).not.toHaveLength(0);
  });
});
