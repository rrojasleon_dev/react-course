import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Greeting from "./Greetings";

// testing suites:
describe("Testing Greeting component", () => {
  test("Testing greeting description", () => {
    // Arrange
    render(<Greeting />);

    // Act
    //...

    // Assert
    const element = screen.getByText("Title");
    expect(element).toBeInTheDocument();
  });

  test("Check for default paragraph text", () => {
    render(<Greeting />);

    const outputEl = screen.getByText("default text");
    expect(outputEl).toBeInTheDocument();
  });

  test("Check for changed text", () => {
    render(<Greeting />);

    // Act
    const button = screen.getByRole("button");
    userEvent.click(button);

    const outputEl = screen.getByText("changed", { exact: false });
    expect(outputEl).toBeInTheDocument();
  });

  test("check for default paragraph not to render after button clicked", () => {
    render(<Greeting />);

    // Act
    const button = screen.getByRole("button");
    userEvent.click(button);

    const outputEl = screen.queryByText("default text", { exact: false });
    expect(outputEl).toBeNull();
  });
});
