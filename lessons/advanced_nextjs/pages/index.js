import { Fragment } from "react";
import Head from "next/head";
import { MongoClient } from "mongodb";
import MeetupList from "../components/meetups/MeetupList";

function HomePage(props) {
  return (
    <Fragment>
      <Head>
        <title>React Meetups with next.js</title>
        <meta
          name="description"
          content="content that shows as description on search result"
        ></meta>
      </Head>
      <MeetupList meetups={props.meetups} />
    </Fragment>
  );
}
// Server-Side rendering
// export async function setServersideProps(context) {
//   // context param have access to request and response
//   const req = context.req;
//   const res = context.res;
//   // server side code runs here
//   return {
//     props: {
//       meetups: DUMMY
//     }
//   }
// }
// statc site generation  only works on page component files
// reserverd function
export async function getStaticProps() {
  // this code is run in the build process
  // execute code that will normally run on the server
  const client = await MongoClient.connect(
    "mongodb+srv://admin:adminPassword1234@learningnextcluster.8stfhm5.mongodb.net/meetups?retryWrites=true&w=majority"
  );
  const db = client.db();
  const meetupsCollection = db.collection("meetups");
  const meetups = await meetupsCollection.find().toArray();
  client.close();
  /**
   * Fetch data from an API.
   * Return an object as the following
   */
  return {
    props: {
      meetups: meetups.map((meetup) => ({
        title: meetup.title,
        address: meetup.address,
        image: meetup.image,
        id: meetup._id.toString(),
      })),
    },
    // incremental static validation in seconds
    revalidate: 10,
  };
}

export default HomePage;
