import { MongoClient, ObjectId } from "mongodb";
import Head from "next/head";
import { Fragment } from "react";
import MeetupDetail from "../../components/meetups/MeetupDetail";

function MeetupDetails(props) {
  const { image, title, address, description } = props.meetupData;

  return (
    <Fragment>
      <Head>
        <title>{title} details</title>
        <meta name="description" content={description}></meta>
      </Head>
      <MeetupDetail
        image={image}
        title={title}
        address={address}
        description={description}
      />
    </Fragment>
  );
}

export async function getStaticPaths() {
  const client = await MongoClient.connect(
    "mongodb+srv://admin:adminPassword1234@learningnextcluster.8stfhm5.mongodb.net/meetups?retryWrites=true&w=majority"
  );
  const db = client.db();
  const meetupsCollection = db.collection("meetups");
  const meetups = await meetupsCollection.find({}, { _id: 1 }).toArray();
  client.close();

  // describe all the possible correct values for the dynamic param (meetupId) in this case.
  // any non-existen value returns a 404 page.
  return {
    fallback: false, // false: we define ALL possible paths. True: we define some paths. Blocking: user does not see anything until page is loaded
    paths: meetups.map((meetup) => ({
      params: { meetupId: meetup._id.toString() },
    })),
  };
}

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId; // queryParams on the url
  // fetch  data for a single meetup
  const client = await MongoClient.connect(
    "mongodb+srv://admin:adminPassword1234@learningnextcluster.8stfhm5.mongodb.net/meetups?retryWrites=true&w=majority"
  );
  const db = client.db();
  const meetupsCollection = db.collection("meetups");
  const selectedMeetup = await meetupsCollection.findOne({
    _id: new ObjectId(meetupId),
  });
  client.close();

  return {
    props: {
      meetupData: {
        id: selectedMeetup._id.toString(),
        title: selectedMeetup.title,
        address: selectedMeetup.address,
        image: selectedMeetup.image,
        description: selectedMeetup.description,
      },
    },
  };
}

export default MeetupDetails;
